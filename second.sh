#!/bin/bash

function get_ip
{
ip=`sudo ifconfig | grep "inet" | head -n1 | cut -d ":" -f2`
echo $ip
}

function get_time
{
time=`date |cut -d " " -f 5`
echo $time
}

function get_hostname
{
host=`who | cut -d " " -f 1`
echo $host
}

#script

n=1
while n=1; do
	echo "pls chose an option:"
	echo "1) IP Address"
	echo "2) System Time"
	echo "3) Username - Host name"
	echo "4) Quit"
	echo "#?"

	read input

	if [[ "$input" = "1" ]]
	then
		
		clear
		get_ip;echo

	elif [[ "$input" = "2" ]]
	then
		clear
		get_time;echo

	elif [[ "$input" = "3" ]]
	then
		clear
		get_hostname;echo
	
	elif [[ "$input" = "4" ]]
	then 
		clear
		echo "Exiting.."
		exit
	else
		clear
		echo "Invalid - pls try again"; echo
	fi
done	

